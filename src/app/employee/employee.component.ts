import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'app-my-employee',
    templateUrl: 'employee.component.html'
})
export class EmployeeComponent {
    firstName = 'Tom';
    lastName = 'Hopkins';
    gender = 'Male';
    age = 20;
}
